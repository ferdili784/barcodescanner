import { BarcodeScanResult } from '@ionic-native/barcode-scanner';

export class ScanResult {
    text: string;
    format: string;
    type: string;
    scanDate: number;
        
    constructor() {
    }
    
    set(jsonData: any)
    {
        this.text = jsonData.type;
        this.format = jsonData.format;
        this.type = jsonData.type;
    }

    apply(scanResult: BarcodeScanResult)
    {
        this.text = scanResult.text;
        this.format = scanResult.format;
        if(this.format == 'EAN_13' || this.format == 'EAN_8' || 
           this.format == 'UPC_E' || this.format == 'UPC_A' ||
           this.format == 'CODE_128' || this.format == 'ITF' ||
           this.format == 'CODE_39'){
            this.type = "Product";
        }
        else if(this.text.indexOf("http://") > -1 || this.text.indexOf("https://") > -1){
            this.type = "Weblink";
        }
        else{
            this.type = "Text";
        }
    }
}