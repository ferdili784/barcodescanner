import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ScanResult } from '../../models/scanResult';
import { BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult } from '@ionic-native/barcode-scanner';
import { Clipboard } from '@ionic-native/clipboard';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  scanHistory: Array<ScanResult> = [];
  shownScanHistory: Array<ScanResult> = [];
  isInfiniteScrollEnable: boolean = true;
  currentIndex: number = 0;

  constructor(public navCtrl: NavController, private barcodeScanner: BarcodeScanner, private clipboard: Clipboard,
              public toastCtrl: ToastController) {
    this.showData(null);
  }

  refreshData(){
    this.shownScanHistory = [];
    this.isInfiniteScrollEnable = true;
    this.currentIndex = 0;
    this.showData(null);
  }

  openScanner(){
    var thisPage = this;
    let options: BarcodeScannerOptions = {
        resultDisplayDuration: 0,
        prompt: 'Tolong arahkan kamera pada QR Code atau Barcode',
        disableSuccessBeep : true
    };
    this.barcodeScanner.scan(options).then((result: BarcodeScanResult) => {
      let scanResult = new ScanResult();
      scanResult.apply(result);
      thisPage.scanHistory.unshift(scanResult);
      thisPage.refreshData();
    }, (err) => {
    });
  }

  showData(infinteScroll){
    let unShownScanHistoryCount = this.scanHistory.length - this.shownScanHistory.length;
    if(unShownScanHistoryCount > 0){
      let endLoop = (unShownScanHistoryCount >= 10) ? 10 : unShownScanHistoryCount;
      let counter = 0;
      while(counter < endLoop){
        this.shownScanHistory.push(this.scanHistory[this.currentIndex]);
        this.currentIndex++;
        counter++;
      }
      if(infinteScroll){
        infinteScroll.complete();
      }
    }
    else{
      this.isInfiniteScrollEnable = false;
    }
  }

  copyToClipboard(scanHistory: ScanResult){
    this.clipboard.copy(scanHistory.text);
    this.presentToast("Copied to clipboard");
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  doInfinite(infinteScroll) {
    var thisPage = this;
    setTimeout(function(){
      thisPage.showData(infinteScroll);
    }, 1000);
  }
}
